import actions from './actions';

const getInitState = () => ({
  backgroundScrapper: {
    data: [],
  },
  scrapper: {
    data: [],
    refreshing: true,
  },
});

export default function appReducer(state = getInitState(), action) {
  switch (action.type) {
    case actions.CHANGE_DATAREFRESH: {
      return {
        ...state,
        scrapper: {
          ...state.scrapper,
          refreshing: !state.scrapper.refreshing,
        },
      };
    }
    default:
      return state;
  }
}
