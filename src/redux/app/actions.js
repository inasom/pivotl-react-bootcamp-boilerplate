const actions = {
  CHANGE_DATAREFRESH: 'CHANGE_DATAREFRESH',

  changeDataRefreshState: () => ({
    type: actions.CHANGE_DATAREFRESH,
  }),
};

export default actions;
